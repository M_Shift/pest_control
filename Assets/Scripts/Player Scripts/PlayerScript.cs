﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour {

	[SerializeField]
	public GameObject jumpes;
	public float moveForce = 20f, jumpForce = 700f, maxvelocity = 4f;

	private bool grounded = true;/*are we on the ground */

	private Rigidbody2D  myBody;
	private Animator anim;

	private bool moveLeft, moveRight;

	

	void Awake(){
		initializeVariables();
		GameObject.Find("Jump Button").GetComponent<Button>().onClick.AddListener(()=> Jump());
	}


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		PlayerWalkJoyStick();
		PlayerWalkKeyboard();
		
	}

	void initializeVariables(){
		myBody = GetComponent<Rigidbody2D>();
		anim = GetComponent<Animator>();

	}

	public void SetMoveLeft(bool moveLeft){
		this.moveLeft = moveLeft;
		this.moveRight = !moveLeft;

	}

	public void StopMoving(){
		this.moveLeft = false;
		this.moveRight = false;
	}

	void PlayerWalkJoyStick(){
		float forceX = 0f;
		float vel =  Mathf.Abs (myBody.velocity.x);

		if(moveRight){
			if(vel < maxvelocity){
				if(grounded){
					forceX = moveForce;
				} else {
					forceX = moveForce *1.1f;
				}
			}

			Vector3 scale = transform.localScale;
			scale.x = 1f;
			transform.localScale = scale;

			anim.SetBool("Walk",true);
		} else if(moveLeft){
			if(vel < maxvelocity){
				if(grounded){
					forceX = -moveForce;
				} else {
					forceX = -moveForce *1.1f;
				}
			}

			Vector3 scale = transform.localScale;
			scale.x = -1f;
			transform.localScale = scale;

			anim.SetBool("Walk",true);
		} else {
			anim.SetBool("Walk",false);
		}

		myBody.AddForce(new Vector2(forceX, 0));
	}

	void PlayerWalkKeyboard(){
		float forceX = 0f;
		float forceY = 0f;

		float vel =  Mathf.Abs (myBody.velocity.x);

		float h = Input.GetAxisRaw("Horizontal");

		if(h > 0){
			if(vel < maxvelocity){
				if(grounded){
					forceX = moveForce;
				} else {
					forceX = moveForce *1.1f;
				}
			}

			Vector3 scale = transform.localScale;
			scale.x = 1f;
			transform.localScale = scale;

			anim.SetBool("Walk",true);


		}else if (h < 0){
			if(vel < maxvelocity){
				if(grounded){
					forceX = -moveForce;
				} else {
					forceX = -moveForce *1.1f;
				}
			}

			Vector3 scale = transform.localScale;
			scale.x = -1f;
			transform.localScale = scale;

			anim.SetBool("Walk",true);

		}else if (h ==0){
			anim.SetBool("Walk",false);
		}

		if(Input.GetKey(KeyCode.Space)){
			if(grounded){
				grounded = false;
				forceY = jumpForce;
			}
		}

		myBody.AddForce(new Vector2(forceX, forceY));
	}

	void OnCollisionEnter2D(Collision2D target){
		if(target.gameObject.tag == "ground"){
			grounded = true;
		}
	}

	void OnTriggerEnter2D(Collider2D target){
		
		float forceY = 0f;
		if(target.gameObject.tag == "SpiderKiller"){
			grounded = false;
			forceY = jumpForce;
			myBody.AddForce(new Vector2(moveForce, 1000));
		}
		if(target.gameObject.tag == "KillSwitch"){
			Debug.Log("Hit Kill Switch");
			GameObject.Find("Gameplay Controller").GetComponent<GameplayController>().PlayerDied();
		}

		//myBody.AddForce(new Vector2(moveForce, 1000));
	}

	public void Jump(){
		if(grounded){
			grounded = false;
			myBody.AddForce(new Vector2(0, jumpForce));
		}
	}
	public void BoucePlayerWithBouncy(float force){
		
			grounded = false;
			myBody.AddForce(new Vector2(0, force));
		
	}

}
