﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour {

	public static DoorScript instance;

	[SerializeField]
	private string nextLevel;
 
	private Animator anim;
	private BoxCollider2D box;
	[HideInInspector]
	public int collectablesCount;

	// Use this for initialization
	void Awake () {
		MakeInstance();
		anim = GetComponent<Animator>();
		box = GetComponent<BoxCollider2D>();
	}

	void MakeInstance(){
		if(instance == null) {
			instance = this;
		}
	}
	
	// Update is called once per frame
	IEnumerator OpenDoor(){
		anim.Play("Open");
		yield return new WaitForSeconds(.7f);
		box.isTrigger = true;
	}

	void OnTriggerEnter2D(Collider2D target){
		if( target.tag == "Player"){
			Debug.Log("Game Finished");
			Application.LoadLevel(nextLevel);
		}
	}

	public void DecrementCollectables(){
		collectablesCount --;

		if (collectablesCount == 0){
			StartCoroutine(OpenDoor());
		}
	}
}
