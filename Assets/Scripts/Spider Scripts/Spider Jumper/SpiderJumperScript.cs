﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderJumperScript : MonoBehaviour {

	public float forceY = 300f;

	private Rigidbody2D mybody;
	private Animator anim;

	void Awake(){
		mybody = GetComponent<Rigidbody2D>();
		anim = GetComponent<Animator>();
	}

	// Use this for initialization
	void Start () {
		StartCoroutine(Attack());
	}

	IEnumerator Attack(){
		yield return new WaitForSeconds(Random.Range(2,7));
		forceY = Random.Range(200f, 550f);

		mybody.AddForce(new Vector2(0, forceY));
		anim.SetBool("Attack", true);

		yield return new WaitForSeconds(.7f);
		StartCoroutine(Attack());
	}

	void OnTriggerEnter2D(Collider2D target){
		if(target.tag == "ground"){
			anim.SetBool("Attack", false);
		}
		if(target.tag == "Player"){
			Destroy(target.gameObject);
			GameObject.Find("Gameplay Controller").GetComponent<GameplayController>().PlayerDied();
		}

	}
	
	
}// spider jumper
