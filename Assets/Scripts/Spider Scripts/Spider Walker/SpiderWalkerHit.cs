﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderWalkerHit : MonoBehaviour {

	[SerializeField]
	private GameObject boss;

	private BoxCollider2D mycollider;

	void Awake(){
		mycollider = GetComponent<BoxCollider2D>();;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter2D(Collider2D target){
		if(target.gameObject.tag == "Player"){
			Destroy(boss);
		}
	}
}
