﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderWalkerScript : MonoBehaviour {

	[SerializeField]
	private Transform startPos, endPos;

	private bool collision;

	public float speed = 1f;
	private Rigidbody2D mybody;

	void Awake(){
		mybody = GetComponent<Rigidbody2D>();

	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Move();
		ChangeDirection();
	}

	void ChangeDirection(){
		collision = Physics2D.Linecast (startPos.position, endPos.position, 1 << LayerMask.NameToLayer("ground"));
		
		Debug.DrawLine(startPos.position, endPos.position, Color.green);

		if(!collision){
			Vector3 temp = transform.localScale;
			if(temp.x ==1f){
				temp.x = -1f;
			}else{
				temp.x =1;
			}

			transform.localScale = temp;
		}
	}

	void Move(){
		mybody.velocity = new Vector2(transform.localScale.x, 0) * speed;
	}

	void OnCollisionEnter2D(Collision2D target){

		if(target.gameObject.tag == "Player"){
			Destroy(target.gameObject);
			GameObject.Find("Gameplay Controller").GetComponent<GameplayController>().PlayerDied();
		}

		if(target.gameObject.tag == "Walker"){

			Vector3 temp = transform.localScale;
			if(temp.x ==1f){
				temp.x = -1f;
			}else{
				temp.x =1;
			}

			transform.localScale = temp;
		}
	}
}
