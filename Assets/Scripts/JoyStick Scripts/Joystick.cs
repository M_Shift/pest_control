﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class Joystick : MonoBehaviour, IPointerUpHandler, IPointerDownHandler {

	private PlayerScript player;

	void Awake(){
		player = GameObject.Find("Player").GetComponent<PlayerScript>();
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnPointerDown(PointerEventData data){
		if(gameObject.name == "Left Button"){
			player.SetMoveLeft(true);
		}else{
			player.SetMoveLeft(false);
		}
	}

	public void OnPointerUp(PointerEventData data){
		player.StopMoving();
	}
}
